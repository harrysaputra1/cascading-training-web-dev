# Cascading Training Web Dev

Sesuai dengan tema workshop, kegiatan ini ditujukan untuk berbagi kepada peserta semua tekait aspek HTML5 dengan cara yang praktis. Selama kegiatan, kita akan membangun situs web menggunakan fitur HTML5 yang paling umum.
Sebagian besar teori dan konsep HTML5 akan dijelaskan disepanjang kegiatan ketika membangun situs web. Oleh karenanya, pada akhir workshop terbentuklah situs web yang berfungsi penuh yang dapat anda gunakan sebagai template untuk web HTML5 anda kedepannya.